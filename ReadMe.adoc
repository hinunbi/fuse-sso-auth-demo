:sectnums:
# Red Hat Fuse RH-SSO 인증 데모 애플리케이션


## 애플리케이션 실행

두 개 RH-SSO Realm 을 이용해 인증 성공 실패를 검증했습니다.

       RH-SSO Realm : wechat
       RH-SSO ClientID : web-popup

       RH-SSO Realm : api-plan-test-by-jcha
       RH-SSO ClientID : 7289ffd9

애플리케이션 실행

    $ mvn clean spring-boot:run

## 인증 테스트

인증 테스트에 사용된 인증 헤더는 아래 RH-SSO Realm 을 이용하 액세스 토큰을 획득했습니다.

  RH-SSO Realm : wechat
  RH-SSO ClientID : web-popup

호출 테스트

    curl -X GET \
      http://localhost:8080/fuse/service \
      -H 'cache-control: no-cache' \
      -d '{"key": "Hello World"}'


    curl -X GET \
      http://localhost:8080/fuse/service \
      -H 'Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJUQ2x2ZVlQYi1lYTdRX3RkOTQ4S09rZWNzQXdISFFLdW1VWGlMTTJuakVVIn0.eyJqdGkiOiIwYTczNDdmMi03OWZiLTRlZDItYmFiMC01MTJhZDg5NjIzMmQiLCJleHAiOjE1NTQyODEzNjQsIm5iZiI6MCwiaWF0IjoxNTU0MjgxMDY0LCJpc3MiOiJodHRwczovL3Nzby1yaHNzby5hcHBzLmtib3MubG9jYWwvYXV0aC9yZWFsbXMvYXBpLXBsYW4tdGVzdC1ieS1qY2hhIiwiYXVkIjoiNzI4OWZmZDkiLCJzdWIiOiI5OTJiNDA2ZS0yNmU0LTQ3OTItYjY0OS0zMmJhNDc5OTg2YWMiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiI3Mjg5ZmZkOSIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6ImVhYmUxYzRkLTQ2YmEtNGZhZC04YWU3LTVhOWNhYTU4NzUyMSIsImFjciI6IjEiLCJhbGxvd2VkLW9yaWdpbnMiOlsiaHR0cHM6Ly9vcGVuaWRjb25uZWN0Lm5ldCJdLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsidW1hX2F1dGhvcml6YXRpb24iXX0sInJlc291cmNlX2FjY2VzcyI6eyJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJrZGhvbmcifQ.XKVCl6K_bO7RLlp7j8Rqa_uHejoRBAA4iKtOeCalEvo1lxmXTzePcCSn3itMY67OgSBPluDbYdh0nKPoU0erx6xSM29bJjvpB9K6LVpK7rIa_xoOAH57Xrwg09ki69CyELsjFgi5MiUHCaEZg9utBSnAoGyWef12F-4V9A91HLXIuKTa0jWpggdXFLXOTLJEOLfVbDxMTeOiLuLRHIOe0XoIn0MScaQmYZUTKO1pkzjke-xBc0rg9LkSjcLHga4VEJEFSfe6psE4V8VwgTv6hZORsDrUS2R-1eEZjmA9hiU78wFO75hpPCfktMRvR5ZgHjiAobsk08VtA6ccpS56qA' \
      -H 'cache-control: no-cache' \
      -d '{"key": "Hello World"}'