package com.shb.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends SecurityConfigSupport {

  @Override
  protected void configure(HttpSecurity http) throws Exception {

    super.configure(http);

    http.authorizeRequests()
        .antMatchers("/fuse/**")
        .authenticated();
  }
}